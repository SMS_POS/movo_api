module Api

  class Viewers < ActiveRecord::Base
    attr_accessor :bouquets

    def self.getId(id)
      where("id = '#{id}'")
    end

    def self.exists?(smartcard_id)
      conditions = {}
      conditions[:viewers_smartcard_id] = smartcard_id
      output = select("viewers_id").where(conditions)
      !output.empty?
    end


    def self.find_active(clientid,phonenumber)
      if (clientid.blank? && phonenumber.blank?) then return [] end
      conditions = {}
      conditions[:viewers_is_active] = 1
      conditions[:viewers_smartcard_id] = clientid unless clientid.blank?
      conditions[:viewers_phonenum] = phonenumber unless phonenumber.blank?
      where(conditions)
    end

    def self.find_all(clientid)
      if (clientid.blank?) then return [] end
      conditions = {}
      conditions[:viewers_smartcard_id] = clientid unless clientid.blank?
      where(conditions)
    end


    def self.getSmartCardId(chipid)
      conditions = {}
      conditions[:viewers_settopbox_id] = chipid
      conditions[:viewers_is_active] = 0
      select("viewers_smartcard_id").where(conditions).first
    end

    def self.produce_xml(builder,fields,data)
      xml.viewers do
        xml.status "ERROR"
        xml.message 'Mandatory parameters not passed or not valid'
      end
    end

    def self.callActivate(viewer)
      self.connection.execute("call proc_activate('#{viewer.chipid}','#{viewer.firstname}','#{viewer.lastname}','#{viewer.phonenum}','#{viewer.nif}','#{viewer.address}','#{viewer.zone}','#{viewer.email}');")
    end



  end


  class Viewers_xml

    def self.produce_xml(xml,fields,datas)
       xml.viewers do
         datas.each do |data|
            xml.viewer do |inner|
                fields[0].each do |key,value|
                  inner.send(value, data[key])
                end
                Viewers_bouquets_xml.produce_xml(xml,fields[1],data.bouquets)
             end
          end
       end
    end

    def self.produce_xml_for_api(xml,fields,input_data)
      xml.viewers do
        input_data.each do |data|
          xml.viewer do |inner|
            fields.each do |key,value|
              inner.send(value, data[key])
            end
          end
        end
      end
    end

  end


end