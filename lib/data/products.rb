module Api

  class Products < ActiveRecord::Base


    def self.find_active
      conditions = {}
      conditions[:products_is_active ] = 1
      where(conditions)
    end

    def self.find_by_id(productid)
      conditions = {}
      conditions[:products_id ] = productid
      where(conditions)
    end


    def self.proc_orderproduct2(parsed_id,parsed_product)
      self.connection.execute("call proc_orderproduct2 (#{parsed_id},#{parsed_product},@returnValue);")
      result = self.connection.select_all("select @returnValue as returnValue;")
      result.first["returnValue"]
    end

    def self.proc_order_mobile_wallet(parsed_id,parsed_product,transid)
      self.connection.execute("call proc_order_mobile_wallet (#{parsed_id},#{parsed_product},\"#{transid}\",@returnValue);")
      result = self.connection.select_all("select @returnValue as returnValue;")
      result.first["returnValue"]
    end


    def self.exists?(product_id)
      conditions = {}
      conditions[:products_hp_id] = product_id
      output = select("products_id").where(conditions)
      !output.empty?
    end



  end


  class Products_xml

    def self.produce_xml(xml,fields,datas)
      xml.products do
        datas.each do |data|
          xml.product do |inner|
            fields[0].each do |key,value|
              inner.send(value, data[key])
            end
          end
        end
      end
    end
  end


end