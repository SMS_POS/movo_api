module Api

  class PaymentResponse
    attr_accessor :paymentLogId,:status

    def initialize(paymentLogId,status)
      @paymentLogId = paymentLogId
      @status = status
    end

    def to_xml(xml)
        xml.Payment do
          xml.PaymentLogId @paymentLogId
          xml.Status @status
        end
    end


  end


end


