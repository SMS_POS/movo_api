ALTER TABLE `payments`
	ADD COLUMN `updated_at` DATETIME NOT NULL AFTER `payment_currency`,
	ADD COLUMN `created_at` DATETIME NOT NULL AFTER `updated_at`,
	ADD COLUMN `processed` INT NULL AFTER `created_at`;